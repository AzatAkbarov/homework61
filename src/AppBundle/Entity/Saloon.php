<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Saloon
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SaloonRepository")
 * @Vich\Uploadable
 * @ORM\Table(name="saloon")
 */
class Saloon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="saloon_image", type="string")
     *
     */
    private $saloon_image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="saloon_image_file", fileNameProperty="saloon_image")
     *
     * @var File
     */
    private $saloonImageFile;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     *
     */
    private $description;


    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="saloon")
     */
    private $dishes;


    /**
     * @param string $saloon_image
     * @return Saloon
     */
    public function setSaloonImage($saloon_image)
    {
        $this->saloon_image = $saloon_image;
        return $this;
    }

    /**
     * @return string
     */
    public function getSaloonImage()
    {
        return $this->saloon_image;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $saloon_image
     * @return Saloon
     */
    public function setSaloonImageFile(File $saloon_image = null)
    {
        $this->saloonImageFile = $saloon_image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getSaloonImageFile()
    {
        return $this->saloonImageFile;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    /**
     * @param Dish $dish
     */
    public function addDish(Dish $dish)
    {
        $this->dishes->add($dish);
    }

    /**
     * @param Dish $dish
     */
    public function removePost(Dish $dish)
    {
        $this->dishes->removeElement($dish);
    }


    /**
     * @param string $description
     * @return Saloon
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $name
     * @return Saloon
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}