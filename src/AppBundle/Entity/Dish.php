<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Dish
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DishRepository")
 * @Vich\Uploadable
 * @ORM\Table(name="dish")
 */
class Dish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     *
     */
    private $price;


    /**
     * @var string
     *
     * @ORM\Column(name="dish_image", type="string")
     *
     */
    private $dish_image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="dish_image_file", fileNameProperty="dish_image")
     *
     * @var File
     */
    private $dishImageFile;

    public function __construct()
    {
        $this->purchases = new ArrayCollection();
    }

    /**
     *
     * @var Saloon
     *
     * @ORM\ManyToOne(targetEntity="Saloon", inversedBy="dishes")
     *
     */
    private $saloon;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Purchase", mappedBy="dish")
     */
    private $purchases;


    /**
     * @param Saloon
     * @return Dish
     */
    public function setSaloon($saloon)
    {
        $this->saloon = $saloon;
        return $this;
    }

    /**
     * @return Saloon
     */
    public function getSaloon()
    {
        return $this->saloon;
    }

    /**
     * @param string $dish_image
     * @return Dish
     */
    public function setDishImage($dish_image)
    {
        $this->dish_image = $dish_image;
        return $this;
    }

    /**
     * @return string
     */
    public function getDishImage()
    {
        return $this->dish_image;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $dish_image
     * @return Dish
     */
    public function setDishImageFile(File $dish_image = null)
    {
        $this->dishImageFile = $dish_image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getDishImageFile()
    {
        return $this->dishImageFile;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Dish
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * @param Purchase $purchase
     */
    public function addPurchase(Purchase $purchase)
    {
        $this->purchases->add($purchase);
    }

    /**
     * @param Purchase $purchase
     */
    public function removePurchase(Purchase $purchase)
    {
        $this->purchases->removeElement($purchase);
    }

    /**
     * @param string $price
     * @return Dish
     */
    public function setPrice(string $price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return integer
     */
    public function getCountPurchases()
    {
        return count($this->purchases);
    }
}