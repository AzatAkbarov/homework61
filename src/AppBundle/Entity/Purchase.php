<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Purchase
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PurchaseRepository")
 * @ORM\Table(name="purchase")
 */
class Purchase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime
     *
     * @ORM\Column(name="date", type="datetime")
     *
     */
    private $date;

    /**
     *
     * @var Dish
     *
     * @ORM\ManyToOne(targetEntity="Dish", inversedBy="purchases")
     *
     */
    private $dish;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $date
     * @return Purchase
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param Dish $dish
     * @return Purchase
     */
    public function setDish(Dish $dish)
    {
        $this->dish = $dish;
        return $this;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

}