<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Saloon;
use Doctrine\ORM\EntityRepository;

/**
 * DishRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DishRepository extends EntityRepository
{
    public function getMostPopularDishesBySaloon(Saloon $saloon, $amount, $from)
    {
        return $this
            ->createQueryBuilder('d')
            ->select('d dish')
            ->addSelect('count(p) amount')
            ->innerJoin('d.purchases', 'p')
            ->groupBy('d')
            ->setMaxResults($amount)
            ->where('d.saloon = :saloon and p.date > :from')
            ->setParameters(['saloon' => $saloon->getId(), 'from' => $from])
            ->orderBy('amount', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
