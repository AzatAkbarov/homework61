<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Saloon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSaloonData extends Fixture
{
    public const SALOON_ONE = 'Ала-Тоо';
    public const SALOON_TWO = 'Фаиза';

    public function load(ObjectManager $manager)
    {
        $saloon1 = new Saloon();
        $saloon1
            ->setName('Ала-Тоо')
            ->setDescription('"Ала-Тоо" — клуб, ресторан, летнее кафе и аквапарк на Южной Магистрали. В меню ресторана, клуба и кафе представлена европейская и национальная кухня')
            ->setSaloonImage('alatoo.jpg');

        $manager->persist($saloon1);

        $saloon2 = new Saloon();
        $saloon2
            ->setName('Фаиза')
            ->setDescription('"Фаиза" - сеть кафе, заслужившая известность далеко за пределами страны, благодаря превосходной кухне, первоклассному обслуживанию и вниманию к каждому гостю')
            ->setSaloonImage('faiza.jpg');

        $manager->persist($saloon2);
        $manager->flush();

        $this->addReference(self::SALOON_ONE, $saloon1);
        $this->addReference(self::SALOON_TWO, $saloon2);
    }
}
