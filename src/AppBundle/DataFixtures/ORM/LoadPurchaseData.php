<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;


class LoadPurchaseData extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $arrayDishes = LoadDishData::getDishes();

        $faker = Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            $purchase = new Purchase();
            $date = $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now');
            $j = floor($i / 100);
            $purchase
                ->setDate($date)
                ->setDish($this->getReference($arrayDishes[$j]));
            $manager->persist($purchase);
        }

        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            LoadDishData::class
        );
    }

}