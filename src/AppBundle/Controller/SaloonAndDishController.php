<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SaloonAndDishController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $saloons = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Saloon')
            ->findAll();

        $saloonsAndPopularDishes = [];

        $date = new \DateTime();
        $from = $date->modify('-1 year');
        foreach ($saloons as $saloon) {
            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getMostPopularDishesBySaloon($saloon, 2, $from);

            $saloonsAndPopularDishes[] = [
                'saloon' => $saloon,
                'dishes' => $dishes
            ];
        }
        return $this->render('@App/SaloonAndDishes/index.html.twig', [
            "saloonsAndDishes" => $saloonsAndPopularDishes
        ]);
    }


    /**
     * @Route("/saloon/{id}/show_detail")
     * @Method("GET")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showSaloonAndHimDishesAction(int $id)
    {
        $saloon = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Saloon')
            ->find($id);

        return $this->render("@App/SaloonAndDishes/showSaloonAndHimDishes.html.twig", [
            'saloon' => $saloon,
            'dishes' => $saloon->getDishes()
        ]);
    }

}
